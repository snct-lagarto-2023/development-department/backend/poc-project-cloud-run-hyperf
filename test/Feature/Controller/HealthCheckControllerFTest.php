<?php

declare(strict_types=1);
/**
 * This file is part of a template project.
 *
 * @link     https://github.com/reinanhs/hyperf-example-app
 * @license  https://github.com/reinanhs/hyperf-example-app/LICENSE
 * @author   @reinanhs
 */

namespace HyperfTest\Feature\Controller;

use HyperfTest\HttpTestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 * @coversNothing
 */
class HealthCheckControllerFTest extends HttpTestCase
{
    /** @dataProvider getHealthCheckRoutes */
    public function testTheApplicationHealthCheckControllerResponse(string $router): void
    {
        /** @var ResponseInterface $response */
        $response = $this->request('GET', $router);

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('Content-Type'));

        $expectContent = [
            'status' => 'ok',
        ];

        $content = json_decode($response->getBody()->getContents(), true);
        $this->assertEquals($expectContent, $content);
    }

    public function getHealthCheckRoutes(): array
    {
        return [
            ['router' => 'health/liveness'],
            ['router' => 'health/readiness'],
        ];
    }
}
