<?php

declare(strict_types=1);
/**
 * This file is part of a template project.
 *
 * @link     https://github.com/reinanhs/hyperf-example-app
 * @license  https://github.com/reinanhs/hyperf-example-app/LICENSE
 * @author   @reinanhs
 */

namespace HyperfTest\Feature\Controller\Demo;

use HyperfTest\HttpTestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

use function Hyperf\Support\env;

/**
 * @internal
 * @coversNothing
 */
class SQLCheckControllerFTest extends HttpTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        if (env('CI_JOB_ID')) {
            $this->markTestSkipped('Execution ignored due to lack of mysql configuration in CI/CD');
        }
    }

    public function testTheApplicationReturnSuccessfulResponse(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->request('GET', 'sql/ping');

        $this->assertEquals(StatusCodes::HTTP_ACCEPTED, $response->getStatusCode());
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('Content-Type'));
    }
}
