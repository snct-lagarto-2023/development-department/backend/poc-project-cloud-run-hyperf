<?php

declare(strict_types=1);
/**
 * This file is part of a template project.
 *
 * @link     https://github.com/reinanhs/hyperf-example-app
 * @license  https://github.com/reinanhs/hyperf-example-app/LICENSE
 * @author   @reinanhs
 */
use App\Formatter\GCPFormatter;
use App\Processor\LogCustomProcessor;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\ProcessIdProcessor;
use Monolog\Processor\PsrLogMessageProcessor;

return [
    'default' => [
        'handler' => [
            'class' => StreamHandler::class,
            'constructor' => [
                'stream' => 'php://stdout',
                'messageType' => ErrorLogHandler::OPERATING_SYSTEM,
                'level' => Level::Info,
            ],
        ],
        'formatter' => [
            'class' => GCPFormatter::class,
            'constructor' => [
                'includeStacktraces' => true,
            ],
        ],
        'PsrLogMessageProcessor' => [
            'class' => PsrLogMessageProcessor::class,
        ],
        'processors' => [
            new LogCustomProcessor(),
            new MemoryUsageProcessor(),
            new ProcessIdProcessor(),
        ],
    ],
    'test' => [
        'handler' => [
            'class' => StreamHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/runtime/logs/hyperf.log',
                'messageType' => ErrorLogHandler::OPERATING_SYSTEM,
                'level' => Level::Debug,
            ],
        ],
        'formatter' => [
            'class' => LineFormatter::class,
            'constructor' => [
                'includeStacktraces' => true,
            ],
        ],
        'PsrLogMessageProcessor' => [
            'class' => PsrLogMessageProcessor::class,
        ],
        'processors' => [
            new MemoryUsageProcessor(),
        ],
    ],
];
