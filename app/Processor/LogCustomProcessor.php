<?php

declare(strict_types=1);
/**
 * This file is part of a template project.
 *
 * @link     https://github.com/reinanhs/hyperf-example-app
 * @license  https://github.com/reinanhs/hyperf-example-app/LICENSE
 * @author   @reinanhs
 */

namespace App\Processor;

use App\Aspect\LoggerAspect;
use Hyperf\Context\ApplicationContext;
use Hyperf\Context\Context;
use Monolog\LogRecord;
use OpenTracing\Span;
use OpenTracing\Tracer;

use const OpenTracing\Formats\TEXT_MAP;

class LogCustomProcessor
{
    public function __invoke(LogRecord $record): LogRecord
    {
        $context = [];
        $root = Context::get('tracer.root');

        if ($root instanceof Span) {
            $container = ApplicationContext::getContainer();
            $trace = $container->get(Tracer::class);

            $trace->inject(spanContext: $root->getContext(), format: TEXT_MAP, carrier: $context);
            $record['extra'] = array_merge((array) $record['extra'], LoggerAspect::processLoggingGoogleAPIs($context));
        }

        return $record;
    }
}
