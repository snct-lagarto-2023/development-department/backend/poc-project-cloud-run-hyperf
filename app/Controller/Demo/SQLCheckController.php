<?php

declare(strict_types=1);
/**
 * This file is part of a template project.
 *
 * @link     https://github.com/reinanhs/hyperf-example-app
 * @license  https://github.com/reinanhs/hyperf-example-app/LICENSE
 * @author   @reinanhs
 */

namespace App\Controller\Demo;

use App\Model\Filme;
use Exception;
use Hyperf\DbConnection\Db;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Request;
use Hyperf\HttpServer\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

#[Controller(prefix: 'sql')]
class SQLCheckController
{
    public function __construct(private LoggerInterface $logger)
    {
    }

    #[RequestMapping(path: 'ping', methods: ['GET', 'HEAD'])]
    public function ping(Response $response): ResponseInterface
    {
        $data = Db::select('SELECT CURRENT_TIMESTAMP()');
        $this->logger->info('A validação da conexão com o banco de dados foi realizada com sucesso.', [
            'ping_data' => $data,
        ]);

        return $response->json(['status' => 'ok', 'data' => $data])
            ->withStatus(StatusCodes::HTTP_ACCEPTED);
    }

    #[RequestMapping(path: 'filme', methods: ['GET', 'HEAD'])]
    public function filme(Request $request, Response $response): ResponseInterface
    {
        $filmeId = $request->input('id', 1);
        $filmeId = intval($filmeId) ?? 1;

        $this->logger->info(sprintf('Iniciando o processo de busca de informações no banco de dados sobre o filme #%d', $filmeId));

        try {
            $filme = Filme::select([
                'filme.titulo as Título do FilmeTd',
                'filme.descricao as Descrição do FilmeTd',
                'filme.ano_de_lancamento as Ano de Lançamento',
                'idioma.nome as Idioma do FilmeTd',
                'filme.duracao_da_locacao as Duração da Locação (em dias)',
                'filme.preco_da_locacao as Preço da Locação',
                'filme.duracao_do_filme as Duração do FilmeTd (em minutos)',
                'filme.custo_de_substituicao as Custo de Substituição',
                'filme.classificacao as Classificação',
                'filme.recursos_especiais as Recursos Especiais',
            ])
                ->leftJoin('idioma', 'filme.idioma_id', '=', 'idioma.idioma_id')
                ->leftJoin('filme_ator', 'filme.filme_id', '=', 'filme_ator.filme_id')
                ->leftJoin('ator', 'filme_ator.ator_id', '=', 'ator.ator_id')
                ->leftJoin('filme_categoria', 'filme.filme_id', '=', 'filme_categoria.filme_id')
                ->leftJoin('categoria', 'filme_categoria.categoria_id', '=', 'categoria.categoria_id')
                ->where('filme.filme_id', $filmeId)
                ->first();

            $this->logger->info('As informações do filme foram recuperadas com sucesso do banco de dados.');
            return $response->json(['status' => 'ok', 'filme' => $filme])
                ->withStatus(StatusCodes::HTTP_ACCEPTED);
        } catch (Exception $exception) {
            $this->logger->error('Ocorreu um erro ao tentar buscar informações do filme no banco de dados', [
                'filme_id' => $filmeId,
                'exception_message' => $exception->getMessage(),
                'exception_trace' => $exception->getTrace(),
                'exception_code' => $exception->getCode(),
            ]);

            return $response->json(['status' => 'error', 'message' => 'O servidor não conseguiu processar sua requisição.'])
                ->withStatus(StatusCodes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
