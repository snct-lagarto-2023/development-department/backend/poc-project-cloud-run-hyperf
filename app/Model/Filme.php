<?php

declare(strict_types=1);
/**
 * This file is part of a template project.
 *
 * @link     https://github.com/reinanhs/hyperf-example-app
 * @license  https://github.com/reinanhs/hyperf-example-app/LICENSE
 * @author   @reinanhs
 */

namespace App\Model;

use Hyperf\DbConnection\Model\Model;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Filme extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'filme';
}
