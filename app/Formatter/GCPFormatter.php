<?php

declare(strict_types=1);
/**
 * This file is part of a template project.
 *
 * @link     https://github.com/reinanhs/hyperf-example-app
 * @license  https://github.com/reinanhs/hyperf-example-app/LICENSE
 * @author   @reinanhs
 */

namespace App\Formatter;

use Monolog\Formatter\JsonFormatter;
use Monolog\LogRecord;

class GCPFormatter extends JsonFormatter
{
    public function format(LogRecord $record): string
    {
        $data = json_encode($this->translateRecordForGoogleCloudLoggingFormat($record));
        return $data . ($this->appendNewline ? "\n" : '');
    }

    protected function formatBatchJson(array $records): string
    {
        $records = array_map(
            function ($record) {
                return $this->translateRecordForGoogleCloudLoggingFormat($record);
            },
            $records
        );
        return json_encode($records);
    }

    protected function translateRecordForGoogleCloudLoggingFormat(LogRecord $record): array
    {
        $formatted = [
            'message' => $record['message'],
            'severity' => $record['level_name'],
            'channel' => $record['channel'],
        ];

        return array_merge((array) $record['context'], (array) $record['extra'], $formatted);
    }
}
