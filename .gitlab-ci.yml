stages:
  - prepare
  - lint
  - test
  - analyze
  - build
  - deploy
  - release

include:
  - template: Code-Quality.gitlab-ci.yml
  - local: .gitlab/ci/*/*.gitlab-ci.yml

variables:
  PHP_IMAGE: reinanhs/php-dev-8.2-swoole:latest
  CODE_QUALITY_DISABLED: 'true'

cache:
  - key:
      files:
        - composer.lock
    paths:
      - vendor/

cache-composer:
  stage: prepare
  image: $PHP_IMAGE
  script:
    - composer install
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

code-quality-psalm:
  stage: lint
  image: $PHP_IMAGE
  dependencies:
    - cache-composer
  needs:
    - cache-composer
  script:
    - php vendor/bin/psalm --output-format=console --show-info=true
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

code-quality-php-cs-fixer:
  stage: lint
  image: $PHP_IMAGE
  dependencies:
    - cache-composer
  needs:
    - cache-composer
  script:
    - php vendor/bin/php-cs-fixer check --diff
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

co-phpunit-unit:
  stage: test
  image: $PHP_IMAGE
  dependencies:
    - cache-composer
  needs:
    - cache-composer
  variables:
    XDEBUG_MODE: coverage
  script:
    - vendor/bin/co-phpunit --prepend test/bootstrap.php -c phpunit.xml \
      --coverage-text \
      --colors=never \
      --coverage-cobertura=build/coverage.cobertura.xml \
      --coverage-clover=build/coverage.xml \
      --coverage-html=build \
      --testsuite=u \
      --log-junit=build/report-unit.xml
  after_script:
    - sed -i "s/\/builds\/snct-lagarto-2023\/development-department\/backend\/${CI_PROJECT_NAME}//g" build/report-unit.xml
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  artifacts:
    paths:
      - build/
    reports:
      junit: build/report-unit.xml
      coverage_report:
        coverage_format: cobertura
        path: build/coverage.cobertura.xml

co-phpunit-feature:
  stage: test
  image: $PHP_IMAGE
  dependencies:
    - cache-composer
  needs:
    - cache-composer
  script:
    - vendor/bin/co-phpunit --prepend test/bootstrap.php -c phpunit.xml --testsuite=f --log-junit=report-feature.xml
  after_script:
    - sed -i "s/\/builds\/snct-lagarto-2023\/development-department\/backend\/${CI_PROJECT_NAME}//g" report-feature.xml
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  artifacts:
    reports:
      junit: report-feature.xml

#sonarqube-check:
#  allow_failure: true
#  retry: 2
#  stage: analyze
#  needs:
#    - co-phpunit-unit
#  dependencies:
#    - co-phpunit-unit
#  image:
#    name: sonarsource/sonar-scanner-cli:latest
#    entrypoint: [ "" ]
#  cache:
#    key: "${CI_JOB_NAME}"
#    paths:
#      - .sonar/cache
#  before_script:
#    - echo "sonar.projectKey=${CI_PROJECT_NAME}" >> sonar-project.properties
#    - echo "sonar.projectName=${CI_PROJECT_NAME}" >> sonar-project.properties
#    - echo "sonar.projectDescription=${CI_PROJECT_DESCRIPTION}" >> sonar-project.properties
#    - echo "sonar.links.ci=${CI_PROJECT_URL}/pipelines" >> sonar-project.properties
#    - echo "sonar.links.issue=${CI_PROJECT_URL}/issue" >> sonar-project.properties
#  script:
#    - sonar-scanner
#  rules:
#    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

code_quality_html:
  extends: code_quality
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [ gl-code-quality-report.html ]
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

build:
  image: google/cloud-sdk:latest
  stage: build
  script:
    - gcloud auth activate-service-account --key-file $GOOGLE_CLOUD_CREDENTIALS
    - gcloud config set project $GCP_PROJECT_ID
    - gcloud builds submit . --config=cloudbuild.yaml --substitutions=REPO_NAME=$CI_PROJECT_NAME,TAG_NAME=$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG

deploy-staging:
  image: google/cloud-sdk:latest
  stage: deploy
  needs:
    - build
  dependencies:
    - build
  script:
    - gcloud auth activate-service-account --key-file $GOOGLE_CLOUD_CREDENTIALS
    - gcloud config set project $GCP_PROJECT_ID
    - gcloud run deploy $CI_PROJECT_NAME
      --platform=managed
      --image=gcr.io/$GCP_PROJECT_ID/$CI_PROJECT_NAME:$CI_COMMIT_TAG
      --region=us-central1
      --concurrency=500
      --min-instances=0
      --allow-unauthenticated
      --set-env-vars="APP_NAME=$CI_PROJECT_NAME"
      --set-env-vars="ZIPKIN_ENDPOINT_URL=$ZIPKIN_ENDPOINT_URL"
      --set-env-vars="TRACER_DRIVER=$TRACER_DRIVER"
      --set-env-vars="GCP_PROJECT_ID=$GCP_PROJECT_ID"
      --set-env-vars="APP_VERSION=$CI_COMMIT_TAG"
      --set-env-vars="DB_HOST=$GCP_SQL_IP"
      --set-env-vars="DB_DATABASE=$CI_PROJECT_NAME"
      --set-env-vars="DB_USERNAME=$CI_PROJECT_NAME"
  environment:
    name: staging
    url: "https://$CI_PROJECT_NAME-b2fbr4ztia-uc.a.run.app"
  rules:
    - if: $CI_COMMIT_TAG

release-job:
  stage: release
  dependencies:
    - deploy-staging
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
      when: manual
  script:
    - echo "running release_job"
  release:
    name: 'Release $CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
    description: '$CI_COMMIT_TAG_MESSAGE'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'Cloud Run Link'
          url: 'https://$CI_PROJECT_NAME-b2fbr4ztia-uc.a.run.app'

update-changelog:
  image: alpine/curl
  stage: release
  script:
    - |
      curl --header "PRIVATE-TOKEN: $GITLAB_CHANGELOG_TOKEN" --data "version=$CI_COMMIT_TAG&branch=$CI_DEFAULT_BRANCH" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/changelog"
  rules:
    - if: $CI_COMMIT_TAG
